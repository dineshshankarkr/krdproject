package com.qa.crm.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.crm.base.TestBase;
import com.qa.crm.pages.HomePage;
import com.qa.crm.pages.LoginPage;

public class HomePageTest extends TestBase {


	LoginPage loginPage ;
	HomePage homePage;
	
	public HomePageTest() {
		
		super(); 
		
	}
	
	
	@BeforeMethod
	  public void setup() {
		   
		   initialization();
		    loginPage = new LoginPage(); 
		    homePage =  loginPage.Login(prop.getProperty("username"),prop.getProperty("password"));
	   }
	
	
	@Test(priority=2)
	public void checkUserNameLabelTest() {
		String HPUserNameLabel = homePage.checkUserNameLabel();
		Assert.assertEquals(HPUserNameLabel, "Dinesh Shankar Kandasamy Rajan");
	}
	
	
	@Test(priority=1)
	public void verifyHomePageTitleTest() {
		String HPTitle = homePage.verifyHomePageTitle();
		Assert.assertEquals(HPTitle, "Cogmento CRM","Home Page Not Matched");
	}
	   
	   @AfterMethod
	   public void teardown() {
	   driver.quit();
		   
	   }
	
	
	
	
	
	
}
