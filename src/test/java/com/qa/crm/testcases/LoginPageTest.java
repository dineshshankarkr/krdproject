package com.qa.crm.testcases;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;

import com.qa.crm.base.TestBase;
import com.qa.crm.pages.HomePage;
import com.qa.crm.pages.LoginPage;

public class LoginPageTest extends TestBase{
	
	LoginPage loginPage ;
	HomePage homePage;
	
	public LoginPageTest() {
		super();
	}
	
   @BeforeMethod
   public void setup() {
	   
	   initialization();
	    loginPage = new LoginPage(); 	    
   }
   
      
   @Test
   public void logintest() {
	 homePage =  loginPage.Login(prop.getProperty("username"),prop.getProperty("password"));
   }
  
      
   @AfterMethod
   public void teardown() {
	  driver.quit();
	   
   }
   
   
   
   
   
}
