package com.qa.crm.testcases;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.crm.base.TestBase;
import com.qa.crm.pages.LoginPage;

public class PDFReaderTest extends TestBase {

	public PDFReaderTest() {
		
		super(); 
		
	}
	
	
	//@BeforeMethod
	  //public void setup() {
		//      initialization();
			//   }
	
	
	@Test
	public void ReadPDFTest() throws IOException {
		
		//driver.get(prop.getProperty("pdflocalpath"));
		
		//String currentUrl  = driver.getCurrentUrl();
		
		
		//System.out.println(currentUrl);
		
		//URL url = new URL(currentUrl);
		
		
		String currentUrl = prop.getProperty("pdflocalpath");
		System.out.println(currentUrl);
		URL url = new URL(currentUrl);
		
		
		InputStream is = url.openStream();
		BufferedInputStream fileParse = new BufferedInputStream(is);
		PDDocument document = null;
		
		document =  PDDocument.load(fileParse);
		String pdfcontent = new PDFTextStripper().getText(document);
		//System.out.println(pdfcontent);
		
		Assert.assertTrue(pdfcontent.contains("Employment History"));
		
		
	}
	
	
	  // @AfterMethod
	   //public void teardown() {
	   //driver.quit();
	   // }
	
	
	
}
