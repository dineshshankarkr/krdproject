package com.qa.crm.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.crm.base.TestBase;

public class HomePage extends TestBase {

	
	//Page Factory - OR
	@FindBy(xpath="//button[text()=\"Show Filters\"]")
	WebElement HPShowFilters;
	
	
	@FindBy(xpath=" //span[contains(@class,'user-display')]")
	WebElement HPusernamelabel;

	
	@FindBy(xpath="//span[contains(text(),'Contacts')]")
	WebElement HPContactsLink;
	
	//Initializing the page objects
	public HomePage(){ 
	PageFactory.initElements(driver, this);
	}

	
	//Actions
	
	public String verifyHomePageTitle() {
		return driver.getTitle();
	}
			
    public String checkUserNameLabel() {
    	return HPusernamelabel.getText();
    }
	
    
    public ContactsPage clickcontactsLink() {
    	HPContactsLink.click();
    	  	return new ContactsPage();
    }
	
}
