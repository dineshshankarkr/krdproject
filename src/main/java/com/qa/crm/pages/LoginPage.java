package com.qa.crm.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.crm.base.TestBase;

public class LoginPage extends TestBase {

	
	//Page Factory - OR
	
	@FindBy(name = "email")
	WebElement EmailAddress;
	
	@FindBy(name = "password")
	WebElement password;
	
	@FindBy(xpath="//div[contains(text(),'Login')]")
	WebElement SubmitBtn;
	
	
	@FindBy(id="top-header-menu")
	WebElement headerlogo;
	
	
	
	//Initializing the page objects
	
	public LoginPage(){ 
		
		PageFactory.initElements(driver, this);
	}

	//Actions
	
	public HomePage Login(String un , String pwd){
		EmailAddress.sendKeys(un);
		password.sendKeys(pwd);
		SubmitBtn.click();
		
		return new HomePage();
		
	}
	
	
	
	
}